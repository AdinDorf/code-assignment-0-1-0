from Board import Board as B
from enum import Enum as E
from Board import States

import pprint


class Token(E):
    O = 0
    X = 1

class TicTacToe():
    
    #Tokens used for your tic tac toe game should be an enum where each token corresponds to 1,2,3,4... increasing for each token.
    def __init__(self, boardSize = 3, Tokens = Token):
       self.game = B(boardSize,Tokens)
       self.TokenSet = Tokens
       self.boardSize = boardSize

    def play(self):

        #Set a player counter. This is just so we can keep track of which token's turn it is.
        player = 0
        
        #Begin iterating until the game is complete
        while not self.game.isFinished():
            self.printGrid()

            attemptTokenPlacement = True
            while (attemptTokenPlacement):

                fin = False
                while not fin:
                    x = input("Player " + str(self.TokenSet(player).name) + " input your column value (grid starts at 1): ")
                    y = input("Player " + str(self.TokenSet(player).name) + " input your row value (grid starts at 1): ")
                    if x.isnumeric() and y.isnumeric():
                        x = int(x)
                        y = int(y)
                        if x <= self.boardSize and y <= self.boardSize and x > 0 and y > 0:
                            fin = True 
                            attemptTokenPlacement = not self.game.setSquare(self.TokenSet(player), y - 1, x - 1)

                    if(not fin):
                        print("Something was wrong with your input. Try again.")
                if attemptTokenPlacement:
                    print("You cannot replace a token that is already on the game board.")

            player += 1
            if player == len(self.TokenSet):
                player = 0

        self.printGrid()

        if self.game.state == States.VICTORY:
            print("The winner is: " + self.game.winner.name)
        else:
            print("Cats game! No one Wins.")

    #Play Testing takes in a file with inputs in the order of: Player 1 x, Player 1 y. Player 2 x, Player 2 y. Player 1 x, Player 1 y...
    def playTesting(self, inputs):
        
        player = 0
        count = 0
 
        while self.game.state == States.INCOM and count < len(inputs) - 1:
            x = int(inputs[count])
            y = int(inputs[count + 1])
            count += 2
            attemptTokenPlacementFailure = not self.game.setSquare(self.TokenSet(player), y - 1, x - 1)
            if attemptTokenPlacementFailure:
                raise Exception("Index " + str(x) + ',' + str(y) +" was used already.")

            player += 1
            if player == len(self.TokenSet):
                player = 0
       
        self.printGrid()

    def printGrid(self):
        #Prep a string to build so we can make a nice, visually appealing representation of the board.
        gridStr = ""
        i = 1

        #For each value in our grid, add it. Then, put a bar after unless it is the last value in a row. (i = boardSize) 
        for val in self.game.iterableGrid():

            if val != -1:
                gridStr += str(self.TokenSet(val).name)
            else:
                gridStr += " "

            if i != self.boardSize * self.boardSize:
                if i % self.boardSize == 0:
                    gridStr += "\n"
                    for x in range(0, self.boardSize):
                        gridStr += "--"
                    gridStr += "\n"
                else:
                    gridStr += "|"

            i += 1
        print(gridStr)

    #This is for testing, so you can check the current game state token and compare with expected.
    def returnGameState(self):
        return self.game.state
    
    # returns the winning token.
    def returnGameWinner(self):
        return self.game.winner

    # returns the Game State.
    def returnGameWinType(self):
        return self.game.winType