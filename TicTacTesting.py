#!/usr/bin/env python3

from Board import States
import TicTacToe
import platform
import yaml


testCase = 1
totalCases = 2
fails = 0


def run_a_test_data(data_id):
    """
    @type data_id: int
    @return 
       - True when test passed, False otherwise.
       - Winner token (O or X).
       - End state (INCOM / CAT)
    """

    if platform.system() == "Linux":
        testStr = "tests/test" + str(data_id) + ".yaml"
    elif platform.system() == "Windows":
        testStr = "tests\\test" + str(data_id) + ".yaml"

    stream = open(testStr,'r')
    test = yaml.load(stream, yaml.CLoader)
    game = TicTacToe.TicTacToe(test["GRID_SIZE"])
    game.playTesting(test["INPUTS"])

    return game, test
