#!/usr/bin/env python3

from TicTacToe import TicTacToe as G

playing = 'yes'

while playing == 'yes':
    game = G()
    game.play()
    playing = input("Type yes to play again? ")

