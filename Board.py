#Author: Jared Perkins
#Date: 5/13/2021

#Numpy imported for use of optimized data organization in the format of a 2 dimensional space with comparison indexing.
import numpy as np
#Enum imported for use in making letter values "x" and "o" to numbers.
from enum import Enum as E

class States(E):
    CAT = 301    # CAT means "tie"
    INCOM = 401  # Stands for "Incomplete"
    VICTORY = 501 # Victory is returned when someone wins

class WinType(E):
    DIAGONAL = 0
    HORIZONTAL = 1
    VERTICAL = 2
    NAN = 3

class Board:
    
    #boardSize is the grid size in YxY format. 
    def __init__(self, boardSize, Tokens):

        #An NP array that represents the current board of the tic tac toe game
        self.grid = np.zeros((boardSize,boardSize))
        self.grid[:] = -1
        self.boardSize = boardSize
        #The current state of the game. If someone wins the game, this variable will update from a States enum value to whatever value is being used
        #as the winning Token. So if no Tokens are set and X wins, this will be Token.X.
        self.state = States.INCOM
        #The type of win that was acheived
        self.winType = WinType.NAN
        #The winning token
        self.winner = None
        #The token set in use for this board. Allows for any number of different tokens to be used.
        self.TokenSet = Tokens

    #input is the grid value you want to put in. It should be one of the Enums given in declaration. location should be a tuple with the (x,y) coordinate of the changed value.
    #returns a boolean value which represents whether or not placement was successful. 
    def setSquare(self, input, x , y):

        if x >= self.boardSize or y >= self.boardSize or x < 0 or y < 0:
            raise IndexError("Index passed to Board was outside the grid.")

        
        placedValue = False

        if self.grid[x,y] == -1:
            self.grid[x,y] = input.value
            #Update the status of the board now that a square has been set.
            self.statusUpdate()
            placedValue = True
                
        return placedValue

           
    def statusUpdate(self):

        notCats = np.any((self.grid == -1))
        if notCats:

            #row win check
            for row in self.grid:

                #Obtain a truth valued array representing whether or not the value of the tokens in the row matches the token value for O or X
                            #parenthesises for readability
                for token in self.TokenSet:
                    tempCheck = (row == token.value)

                    #check if the truth value array for either token is all true, thus resulting in a board state of a win
                    if np.all(tempCheck):
                        self.state = States.VICTORY
                        self.winType = WinType.HORIZONTAL
                        self.winner = token

            #Column win check
            column = 0
            while column < self.grid.shape[1]:

                for token in self.TokenSet:
                    tempCheck = (self.grid[:,column] == token.value)

                    if np.all(tempCheck):
                        self.state = States.VICTORY
                        self.winType = WinType.VERTICAL
                        self.winner = token

                column += 1


            #Diagonal win Checks
            # check 1: Left-top to right-bottom
            diag = self.grid.diagonal()
            for token in self.TokenSet:
                tempCheck = (diag == token.value)


                if np.all(tempCheck):
                    self.state = States.VICTORY
                    self.winType = WinType.DIAGONAL
                    self.winner = token
            
            # check 2: Right-top to left-bottom.
            diagOther = np.flipud(self.grid).diagonal()
            for token in self.TokenSet:
                tempCheck = (diagOther == token.value)

                if np.all(tempCheck):
                    self.state = States.VICTORY
                    self.winType = WinType.DIAGONAL
                    self.winner = token
        else:
            self.state = States.CAT

    
    def isFinished(self):
        return self.state != States.INCOM

    #setBoard is meant for testing, and takes in a numpy array with values of Token Enum above. setBoard WILL call 
    #statusUpdate after updating the board so that the game state always matches the board. 
    def setBoard(self, npBoard):
        self.grid = npBoard
        self.statusUpdate()

    def iterableGrid(self):
        return self.grid.flatten()

    def __str__(self):
        return  "Grid: " + str(self.grid) + "\n\n" + "State: " + str(self.state)
