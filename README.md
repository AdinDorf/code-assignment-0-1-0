# Plus One Robotics: Coding Assignment for SDET

[[_TOC_]]

NOTE: To read this document, use web browser, or a tool called "`typora`", or any tool that can render "markdown / md" format, to get full visual effect.

## What is the assignment?

This assignment is the code challenge for the Software development engineer in test (SDET) position at Plus One Robotics (POR).

This package includes a `tic-tac-toe` application. Your task is to write test cases for the application and share the modified package on `gitlab.com`.

### Assignment workflow

1. POR will send you the assignment package (you're reading its instruction, so you must've received it by now).
1. Follow the instructions below to complete the assignment.
1. Upload your solution on `gitlab.com`, as a "repository". If you don't have an account, create one for free.
1. Send the URL of the repository to the point of contact at POR.

### Requirement of the assignment
#### Assigned tasks

- Task-1. Create test cases to cover the given scenarios:
   1. Test that X can win on the Diagonal.
   1. Test that X can win on the Horizontal. 
   1. Test that X can win on the Vertical.
   1. Test that Y can win on the Diagonal.
   1. Test that Y can win on the Horizontal.
   1. Test that Y can win on the Vertical.
   1. Test that the game can register Incomplete if not enough input is given.
   1. Test that the game can register a cat's game if there is a tie. 
- Task-2. Add the following validation as the new test cases:
   1. Provide a sample game that attempts to overrwrite a filled square, assert that the game terminates with error.
   1. Provide a sample game that places out-of-bound location, assert that the game terminates with error.
- Task-3. Add new test cases of your own. Free-minded.

#### Other misc. requirements
- Submission
   - 1. All test cases must be executed with `pytest`, and pass.
   - 2. Upon submitting your repository, the Gitlab CI must pass. Gitlab CI should show green. POR may not accept submission before CI passes.
- System environment.
   - 1. Host operating system (i.e. laptop or desktop computer that you use to do this assignment) can be anything, as long as:
      - Docker Engine works. Code execution to take place inside a Docker container. See [docs.docker.com](https://docs.docker.com/engine/install/) for installing Docker engine.
         - This document assumes the application runs inside of a Docker container, which runs on  **Linux** operating system. Note that at POR, Ubuntu Linux is used though, Debian Linux is used in this instruction for this code assignment (for the ease of access to the Docker image).
      - Shell terminal. In this entire instruction, `bash` is used.
   - 2. Programming language: This assignment is designed to be done by **Python**  3.6 or larger.
   - 3. Assert the test cases using a testing runner "`pytest`".
- Logistics
   - 1. Your solution is supposed to be shared on a code sharing cloud site `gitlab.com`.
   - 2. Record the modification using `git`.

## Setting up environment

1. On your host computer, open a terminal.
1. Un-archive the package (archive file) you received from POR at a directory of your choise. Un-archiving it will make a `git` repo called `code-assignment`.
1. On a terminal, Execute the following commands.
   TIP: In the entire this document, command prompt "`$`" for host OS and "`#`" for inside a Docker container is printed, to make distinguishing whether the command is for host or container easy, but when copy-pasting these commands, do not include the prompt.
   ```
   $ export DOCKER_IMG=python:3.8.16-slim-bullseye
   $ export PATH_REPO=%Copy-paste here the absolute path to the code-assignment repo on your host%
   $ docker run -it -v $PATH_REPO:/code-assignment $DOCKER_IMG bash
   ```
   NOTE: The Docker image, [python:3.8.16-slim-bullseye (hub.docker.com)](https://hub.docker.com/_/python/tags?page=1&name=bullseye), is a Debian Linux (version Bullseye) that bundles Python 3.8.
1. Now you're in a Docker container (notice the prompt is `#`), install the dependency of the code-assignment application.
   ```
   # cd /code-assignment
   # pip3 install -r requirements.txt
   # apt update && rosdep init && rosdep update && rosdep install -r -y --from-paths . --ignore-src
   ```

You're ready to run the application.

## Running the application

Do this and if you see the 3 x 3 matrix on the same terminal like below, application is functioning. You can play around  :)
```
# cd /code-assignment
# ./Main.py 
 | | 
------
 | | 
------
 | | 
Player O input your column value (grid starts at 1):
```

## Test cases

### Validate the test data sets

There are test cases defined in [test_tictac.py](./test_tictac.py).

Run them by using `pytest`.

```
# cd /code-assignment
# pytest
```

At the end of execution, the command shows the result of all the test cases it finds by printing something like this.

```
# pytest

:

============ short test summary info ============
============ 2 passed in 0.21s =============
```
In the example above, you see 2 test cases passed. These are the example test cases.

### Application API

The function you will use to test the application is from TicTacTesting.py, and takes in a testID and outputs a game state along with the ground truth 
for that state for comparison. 

- input: 
   - data_id - A string that represents the ID after the word test in the "tests" folder. E.g. For `test1.yaml` file, this value will be `1`.

- output:
   - game - A python object with the following possible returns to check:

      | Name    | Type         | Values                                   |
      |---------|--------------|------------------------------------------|
      | State   | Enum         | "CAT", "INCOM", "VICTORY"                |
      | Winner  | Enum or None | "O", "X", or None                        |
      | WinType | Enum         | "DIAGONAL","HORIZONTAL","VERTICAL","NAN" |

   - test - A python Dict that holds the values for the keys within the test#.yaml passed to the test.

### Writing test cases

Each test case comes with a file and a function:
- A Python test case ([test_tictac.py](./test_tictac.py))
- A sample game ground truth and parameters (stored in [tests](./tests))

Steps to add a test case:
1. Open [test_tictac.py](./test_tictac.py) in an code editor of your choice.
1. If you're adding a new test case: 
   1. Add a method (Follow other test cases in the same file). Make sure the method name begins with `test`, so that `pytest` can detect it.
   1. Add sample game test file. You can find other files of this kind in the folder [tests](./tests). Each file name MUST begin with "`test`", which must be prepended by the test case ID. E.g. Test case 1 is associated with a file `tests/test1.yaml`. Format of the file content:
      - E.g. Content of `tests/test1.yaml`:
         ```
         GRID_SIZE: 3

         INPUTS: [1,1,
                  1,2,
                  1,3,
                  2,1,
                  2,2,
                  2,3,
                  3,3]

         STATE: "VICTORY"
         WINNER: "O"
         WINTYPE: "DIAGONAL"
         ```
         
         This example reads:
         1. Player O placed on col: 1, row: 1.
         1. Then player X placed on col: 1, row: 2.
         1. Then player O placed on col: 1, row: 3.
         1. Then player X placed on col: 2, row: 1.
         1. Then player O placed on col: 2, row: 2.
         1. Then player X placed on col: 2, row: 3.
         1. Lastly, player O placed on col: 3, row: 3.
      - After the last placement, or a player wins, the game terminates.
   1. The rest of the file content options are:
      - `STATE` represents the ground truth final state of the game test.
      - `WINNER` represents the token/player that won the game.
      - `WINTYPE` Is how the token/player won the game.

Note: If you're able to find a bug within the TicTacToe program, reproduce it with a test made following the steps above, place the test file in a **seperate** folder called "bugs", and put the pytest function in a file called test_bugs.py.

## Submission

1. Create a repository on your personal gitlab.com account space, name the repo the same "`code-assignment`".
1. Upload your entire local git repo onto your remote repo on gitlab.com.
1. Make sure Gitlab CI pass as that is a requirement of the submission. Gitlab configuration is already made, you shouldn't have to change anything in order to start using Gitlab CI.

## Troubleshooting, Contact

Sends questions about this assignment via Email to (yes it's very long, but is a valid email address):

```
contact-project+plusone-robotics-product-quality-assurance-code-assignment-41995494-issue-@incoming.gitlab.com
```

EoF
