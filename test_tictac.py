from Board import States
import TicTacTesting
from TicTacToe import Token
import pytest

def run_a_test(id):
    """
    @note: I wanted to preserve the note/summary for each indivdual test,
    however adding an extra 10 lines to each was redundant. There's probably still a better way to do this, 
    perhaps adding the comments into the yaml file itself would be better than looping through the number of cases,
    but this leaves room to have tests of a different format all in 1 file
    @summary: asserts the expected state winner and wintype from the yaml file
    """
    game, test = TicTacTesting.run_a_test_data(data_id=id)

    assert game.returnGameState().name == test["STATE"]
    if game.returnGameWinner():
        assert game.returnGameWinner().name == test["WINNER"]
    else:
        assert not test["WINNER"]
    assert game.returnGameWinType().name == test["WINTYPE"]

def test_case_1():
    """
    @note: This is an example test case provided by Plus One.
    @summary: Assert the game ends with incomplete state.
    """
    run_a_test(1)

def test_case_2():
    """
    @note: This is from the README.md to figure out how to write test cases
    @summary: Assert a victorious game state from O, winning through a diagonal
    """
    run_a_test(2)

def test_case_3():
    """
    @note: Written by Adin Dorf
    @summary: Assert a victorious game state from O, winning through a different diagonal
    """
    run_a_test(3) 

def test_case_4():
    """
    @note: Written by Adin Dorf
    @summary: Assert a victorious game state from O, winning through tne bottom horizontal
    """
    run_a_test(4)

def test_case_5():
    """
    @note: Written by Adin Dorf
    @summary: Assert a victorious game state from O, winning through the middle horizontal
    """
    run_a_test(5)

def test_case_6():
    """
    @note: Written by Adin Dorf
    @summary: Assert a victorious game state from O, winning through the top horizontal
    """
    run_a_test(6)

def test_case_7():
    """
    @note: Written by Adin Dorf
    @summary: Assert a victorious game state from O, winning through the rightmost vertical
    """
    run_a_test(7)

def test_case_8():
    """
    @note: Written by Adin Dorf
    @summary: Assert a victorious game state from O, winning through the rightmost vertical
    """
    run_a_test(8)

def test_case_9():
    """
    @note: Written by Adin Dorf
    @summary: Assert a victorious game state from O, winning through the rightmost vertical
    """
    run_a_test(9)

def test_case_10():
    """
    @note: Written by Adin Dorf
    @summary: Assert a victorious game state from X, winning through a diagonal
    """
    run_a_test(10)

def test_case_11():
    """
    @note: Written by Adin Dorf
    @summary: Assert a victorious game state from X, winning through a different diagonal
    """
    run_a_test(11)
    
def test_case_12():
    """
    @note: Written by Adin Dorf
    @summary: Assert a victorious game state from X, winning through tne bottom horizontal
    """
    run_a_test(12)
    
def test_case_13():
    """
    @note: Written by Adin Dorf
    @summary: Assert a victorious game state from X, winning through the middle horizontal
    """
    run_a_test(13)

def test_case_14():
    """
    @note: Written by Adin Dorf
    @summary: Assert a victorious game state from X, winning through the top horizontal
    """
    run_a_test(14)

def test_case_15():
    """
    @note: Written by Adin Dorf
    @summary: Assert a victorious game state from X, winning through the rightmost vertical
    """
    run_a_test(15)

def test_case_16():
    """
    @note: Written by Adin Dorf
    @summary: Assert a victorious game state from X, winning through the rightmost vertical
    """
    run_a_test(16)

def test_case_17():
    """
    @note: Written by Adin Dorf
    @summary: Assert a victorious game state from X, winning through the rightmost vertical
    """
    run_a_test(17)

def test_case_18():
    """
    @note: Written by Adin Dorf
    @summary: Assert a cat's game gamestate. There are no more valid spaces, and the winner is null.
    """
    run_a_test(18)

def test_case_19():
    """
    @note: Written by Adin Dorf
    @summary: Check that the program returns an exception when you attempt to overwrite an existing square
    """
    #The instructions ask to assert that the game terminates, but that's not what the game is programmed to do
    #It's instead programmed to return a generic Exception, so that's what I check for
    with pytest.raises(Exception):
        game, test = TicTacTesting.run_a_test_data(data_id=19)

def test_case_20():
    """
    @note: Written by Adin Dorf
    @summary: Check that the program returns an exception when you attempt to place a piece in an invalid location outside the board
    """
    #as with the previous test, the game is programmed to throw an exception and continue running, so I check for that exception
    #this time, it's an index error since I'm attempting to access an index outside of the specified list
    with pytest.raises(IndexError): 
        game, test = TicTacTesting.run_a_test_data(data_id=20)

def test_case_21():
    """
    @note: Written by Adin Dorf
    @summary: Check that the program returns an exception when you attempt to input a letter
    """

    with pytest.raises(ValueError):
        game, test = TicTacTesting.run_a_test_data(data_id=21)

def test_case_22():
    """
    @note: Written by Adin Dorf
    @summary: Max out the limit for integer string conversion and see what happens
    """
    #This returns a ValueError instead of an IndexError which is fun   
    with pytest.raises(ValueError):
        game, test = TicTacTesting.run_a_test_data(data_id=22)
   
 
def test_case_23():
    """
    @note: Written by Adin Dorf
    @summary: Attempt to complete a game using unicode character codes instead of ints as input
    """
     #Since TicTacToe.py uses .isnumeric to filter input, you can use things like hexidecimal or binary to send input 
    run_a_test(23)

def test_case_24():
    """
    @note: Written by Adin Dorf
    @summary: Attempt to pass in bits instead of ints
    """
    run_a_test(24)

def test_case_25():
    """
    @note: Written by Adin Dorf
    @summary: Attempt to pass in a bunch of extra whitespace with the integers by passing them as strings/chars
    """
    run_a_test(25)

def test_case_26():
    """
    @note: Written by Adin Dorf
    @summary: Test an incomplete 5x5 game of tic-tac-toe with one player already having 3 in a row to ensure the game doesn't end early
    """
    run_a_test(26)

def test_case_27():
    """
    @note: Written by Adin Dorf
    @summary: Test a completed 5x5 game
    """
    run_a_test(27)

def test_case_28():
    """
    @note: Written by Adin Dorf
    @summary: Ensure the game doesn't end with 4 in a row in a 5x5 game
    """
    run_a_test(28)